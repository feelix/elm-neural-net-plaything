# README #

Created under a standard 0.18 install of Elm. Added the Svg package and tested locally using elm-reactor.

Allows multi-layer artificial neural networks to be defined and visualised performing feed forward and back propagation operations.

Define a network as a comma separated list of layer sizes - so 2,2,1 will be a 3 layer net with 2 input, 2 hidden and one output neuron - ideal for trivial testing of simple Boolean operations. A target layer of the same size as the output layer will also be created.

Bias neurons will be added to each layer, except input, these will always have an activation of 1.0 and their outgoing weights set to initial values in the same way as other connections.

Weights for each neuron's incoming connections will be set to random floats from a normal (Gaussian) distribution with mean 0 and standard deviation 1/(sqrt fanIn), where fanIn is the number of incoming connections, including the connection from a bias neuron.

Input layer neurons can be toggled with mouse clicks, so can those of a target layer. This allows the input neuron's values to be fed forward (FF) through the layers of the network, then the resulting activation of each output neuron can be compared with it's corresponding target neuron and an error value calculated and back propagated (BP) and the weights adjusted to reduce the error.

As the FF and BP actions are performed the visualisation will show each neuron's activation as a shade of red, and the weights connecting neurons as blue or red for -ve and +ve weights respectively.

##Animated Visualisation - Learn##
Learn mode is interesting in that it will go through a specified number of runs of FF and BP learning whatever the inputs and target outputs are even as the user keeps changing them!

The interesting lesson from this mode is you can see how quickly, with a learning rate of 1, the Ann adjusts itself to after a change in either the expected output or the given inputs.

Might just be good to explore this with a view to dynamically controlling a homing pigeon - ie a steering Ann?

##Animated Visualisation - Supervised Learning##
This is the proper learning mode in that during each run, a lesson will be taught to the network, a new set of random inputs will be generated, the expected target values determined according to the 'operator' selected by the user and then one FF/BP cycle will be performed.

Currently a [2,3,1] ANN can be taught in under 1000 lessons the following set of available operators: AND, OR, NAND, and the famously tricky XOR! FOr XOR, the initial set of random weights need to be checked by eye to ensure each input neuron does have a mix of positive and negative outgoing weights - it appears that not all small sets of normally distributed random numbers are equally good!   

Hmm - perhaps I should see if it can do XOR with a [2,2,1] ANN...

These operators work with multiple boolean inputs to give only a single boolean output. The following have multiple outputs:

###Other Operators###
- INVERT, which switches bits, so 0101 -> 1010, but I can't build a net to solve this one! Need to look into this!  
- PARITY, count 1's in input, show 1 if even, 0 if odd. *not yet*
- half adder [00->00, 01->10, 10->10, 11->01]. *not yet*

###Truth Table###
A simple table showing the current training input and the output as 1's and 0's, although where the output neuron's value is indeterminate (ie not within %accuracy of 1 or 0) then it's actual value will be shown.


##Have a Play##
To experiment:
1 specify the layer sizes - 2,3,1 is a good place to start
2 set #runs to say 1000
3 pick an operator
4 click supervised learning
Each FF/BP cycle is now updating the display, so you should see how the model changes as it learns. The truth table stops showing indeterminate values as the operator is learnt, at this point clicking 'supervised learning' again (should - sometimes it doesn't, don't know why, yet ;) stop the cycle at the end of the current lesson, showing how many runs remain from the initial 1000.

Now you might want to experiment with different numbers of layer and different layer sizes.

Also fun is going free-style and trying to drive the ANN mad using the learn mode. Here you get to toggle the input neuron's values, and set the target neuron's values as the net is going through #runs cycles of FF/BP. Teach it some random pattern, then keep changing target neurons - see how quickly the network learns the new target.

If, like me, you find this fun then you are a Geek ;-)

##Discussion##

###Using Animation.Frame diffs###
Having thought about how to use this for a week or so, I finally got space to sit down and convert my super-fast learning cycle hack into a properly synchronised one using the animation library.

Easiest thing I have yet done in Elm! And the result was instantly appealing - real-time learning with every single iteration (FF, BP) visualised.

###Sourcing Random Numbers###
Rather than have a random number generator spit out commands, I decided to use Random.step to manually produce the random inputs on each frame of the animation. This means subscribing to the diffs event - which should be unsubscribed from when no longer learning.

I could stay subscribed to the diffs and trigger a task to produce the random numbers required and use them when the task completes - probably before the next frame, this would save me holding seed in the model and might be tidier.
