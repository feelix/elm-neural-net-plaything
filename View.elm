module View exposing (..)

import Ann exposing (..)
import Model exposing (..)
import Update exposing (..)
import Checker exposing (..)
import Visualisation exposing (..)
import TruthTable exposing (..)
import Lesson exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)


-- import Svg.Attributes exposing (..)
-- import Svg.Events exposing (..)


view : Model -> Html Msg
view model =
    div []
        [ input [ placeholder "Layer Sizes", onInput Model.Sizes ] []
        , button [ Html.Events.onClick Model.FF ] [ Html.text "FF" ]
        , button [ Html.Events.onClick Model.BP ] [ Svg.text "BP" ]
        , button [ Html.Events.onClick Model.Reset ] [ Svg.text "Reset" ]
        , input [ value (toString model.runs), onInput Model.Runs ] []
        , button [ Html.Events.onClick Model.Learn ] [ Svg.text "Learn" ]
        , button [ Html.Events.onClick Model.SupervisedLearning ] [ Svg.text "Supervised Learning" ]
        , select
            [ onInput Operator ]
            [ option [ value "1" ] [ Html.text (opName 1) ]
            , option [ value "2" ] [ Html.text (opName 2) ]
            , option [ value "3" ] [ Html.text (opName 3) ]
            , option [ value "4" ] [ Html.text (opName 4) ]
            , option [ value "5" ] [ Html.text (opName 5) ]
            , option [ value "6" ] [ Html.text (opName 6) ]
            , option [ value "7" ] [ Html.text (opName 7) ]
            ]
        , (truth model)
        , (check model)
        , modelSvg "0, 0, 500, 500" model
        , div [] [ Html.text (toString model) ]
        ]
