module Network exposing (annFF, annBP, annLearn)

import Dict exposing (..)
import Types exposing (..)
import Ann exposing (..)


-- ========== Back Propogate ===========


deltaOutWRTSum : Neuron -> Float
deltaOutWRTSum neuron =
    let
        activation =
            value neuron
    in
        activation * (1 - activation)


deltaSumWRTConnectedNeuron : ANN -> Connection -> Float
deltaSumWRTConnectedNeuron ann connection =
    case Dict.get (fromDescriptor connection) ann of
        Just n ->
            value n

        Nothing ->
            -- error really
            0


gradient : ANN -> Error -> Neuron -> Connection -> Gradient
gradient ann error neuron connection =
    error * (deltaOutWRTSum neuron) * (deltaSumWRTConnectedNeuron ann connection)


adjustConnectionWeight : ANN -> Error -> LearningRate -> Neuron -> Connection -> Connection
adjustConnectionWeight ann error learningRate neuron connection =
    case connection of
        Connection weight descriptor ->
            let
                grad =
                    gradient
                        ann
                        error
                        neuron
                        connection
            in
                Connection (weight - ((Debug.log "Grad=" grad) * learningRate)) descriptor


neuronBP : ANN -> ANN -> Error -> LearningRate -> Neuron -> ANN
neuronBP ann adjusted error learningRate neuron =
    case neuron of
        Neuron value descriptor connections ->
            let
                _ =
                    Debug.log "NeuronBP with error=" error

                _ =
                    Debug.log " for Neuron at = " descriptor
            in
                connections
                    |> List.map (adjustConnectionWeight ann error learningRate neuron)
                    |> Neuron value descriptor
                    |> (flip addNeuron) adjusted


error : Target -> Descriptor -> Neuron -> Error
error target ( layer, position ) neuron =
    let
        nv =
            value neuron

        tv =
            value (Maybe.withDefault neuron (Dict.get ( layer + 1, position ) target))
    in
        nv - tv


annBP : ANN -> Target -> ANN
annBP ann target =
    let
        ( feed, output ) =
            layersAndOutput ann

        _ =
            Debug.log "partitioned output layer = " output

        _ =
            Debug.log "partitioned feed layer = " feed

        errors =
            Dict.map (error target) output
                |> Debug.log "ERRORS: "

        errorsSum =
            Dict.foldl (\key error sum -> sum + error) 0 errors
                |> Debug.log "ERRORSSUM: "

        adjustedOutput =
            Dict.foldl (\key neuron out -> neuronBP ann out (Maybe.withDefault 0 (Dict.get key errors)) 1.0 neuron) Dict.empty output
                |> Debug.log "ADJOP: "

        adjustedFeed =
            Dict.foldl (\key neuron feed -> neuronBP ann feed errorsSum 1.0 neuron) Dict.empty feed
                |> Debug.log "ADJFEED: "
    in
        Dict.union adjustedFeed adjustedOutput



--- =========== Feed Forward ===========


sigmoid : Value -> Value
sigmoid value =
    1 / (1 + e ^ (-1 * value))


weightAndValue : ANN -> Connection -> ( Weight, Value )
weightAndValue ann connection =
    case connection of
        Connection weight descriptor ->
            let
                neuron =
                    Dict.get descriptor ann
            in
                case neuron of
                    Just n ->
                        ( weight, value n )

                    Nothing ->
                        Debug.crash "descriptor is for neuron not in dictionary" descriptor


nIns : ANN -> Neuron -> List ( Weight, Value )
nIns ann neuron =
    case neuron of
        Neuron v d connections ->
            connections
                |> List.map (weightAndValue ann)


nFF : ANN -> Neuron -> List ( Weight, Value ) -> ANN
nFF ann neuron inputs =
    case inputs of
        [] ->
            -- no change for this neuron, so leave it alone
            ann

        ins ->
            let
                value =
                    List.foldl (\( w, v ) s -> s + (w * v)) 0 ins
                        |> sigmoid

                new =
                    Neuron value (descriptor neuron) (connections neuron)
            in
                Dict.insert (descriptor new) new ann


annFF : ANN -> ANN
annFF ann =
    Dict.foldl
        (\k n d -> nFF d n (nIns d n))
        ann
        ann


annLearn : Target -> ANN -> ANN
annLearn target ann =
    annFF ann
        |> (flip annBP) target
