module Types exposing (..)

import Dict exposing (..)
import Random exposing (..)


-- ========== ANN ==========
-- Data types, constructors and accessors


type alias Value =
    Float


type alias Weight =
    Float


type alias Error =
    Float


type alias LearningRate =
    Float


type alias Gradient =
    Float


type alias Layer =
    Int


type alias Position =
    Int


type alias Descriptor =
    ( Layer, Position )


type Connection
    = Connection Weight Descriptor


type alias Connections =
    List Connection


type Neuron
    = Neuron Value Descriptor Connections


type alias ANN =
    Dict.Dict Descriptor Neuron


type alias Target =
    -- Dict.Dict Descriptor Value
    Dict.Dict Descriptor Neuron


type State
    = Truthy
    | Falsey
    | Unknown Value


{-| percentage
-}
type alias Accuracy =
    Int


margin : Accuracy -> Float
margin acc =
    (toFloat acc) / 100.0


state : Accuracy -> Value -> State
state acc value =
    let
        err =
            margin acc
    in
        if (value >= (1.0 - err)) then
            Truthy
        else if (value <= err) then
            Falsey
        else
            Unknown value


fromDescriptor : Connection -> Descriptor
fromDescriptor c =
    case c of
        Connection _ descriptor ->
            descriptor


fromWeight : Connection -> Float
fromWeight c =
    case c of
        Connection weight _ ->
            weight


descriptor : Neuron -> Descriptor
descriptor n =
    case n of
        Neuron _ descriptor _ ->
            descriptor


connections : Neuron -> Connections
connections n =
    case n of
        Neuron _ _ connections ->
            connections


value : Neuron -> Value
value n =
    case n of
        Neuron value _ _ ->
            value
