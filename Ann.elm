module Ann exposing (..)

import Types exposing (..)
import Dict exposing (..)
import Random exposing (..)
import Random.Float exposing (..)
import Array exposing (..)


-- =========== Simple Helpers ===========


layerSize : ANN -> Int -> Int
layerSize ann layer =
    ann
        |> layerDescriptors layer
        |> List.length


outputLayer : ANN -> Int
outputLayer ann =
    ann
        |> Dict.keys
        |> List.foldl
            (\( l, _ ) out ->
                if (l > out) then
                    l
                else
                    out
            )
            0


outputLayerSize : ANN -> Int
outputLayerSize ann =
    layerSize ann (outputLayer ann)


inputLayerSize : ANN -> Int
inputLayerSize ann =
    (layerSize ann 1) - 1


layersAndOutput : ANN -> ( ANN, ANN )
layersAndOutput ann =
    let
        olayer =
            outputLayer ann
                |> Debug.log "output layer = "
    in
        Dict.partition (\( l, _ ) _ -> l /= olayer) ann


layerDescriptors : Layer -> ANN -> List Descriptor
layerDescriptors layer ann =
    ann
        |> Dict.keys
        |> List.filter (\( l, _ ) -> l == layer)


layerNeurons : ANN -> Layer -> List Neuron
layerNeurons ann layer =
    Dict.filter (\( l, _ ) v -> l == layer) ann
        |> Dict.toList
        |> List.map (\( k, v ) -> v)


outputLayerNeurons : ANN -> List Neuron
outputLayerNeurons ann =
    layerNeurons ann (outputLayer ann)



-- ========== Connecting Neurons to others ==========
-- Neuron connects with all neurons in previous layer


addConnectionFromTo : Descriptor -> Neuron -> Neuron
addConnectionFromTo source neuron =
    case neuron of
        Neuron v d c ->
            (Connection 0.01 source)
                :: c
                |> Neuron v d


connectToLayer : ANN -> Neuron -> Int -> Neuron
connectToLayer ann neuron layer =
    let
        descriptors =
            layerSize ann layer
                |> List.range 1
                |> List.map (\n -> ( layer, n ))
    in
        case neuron of
            Neuron v d c ->
                List.foldl (\desc n -> addConnectionFromTo desc n) neuron descriptors


connectToBiasFromNeuron : Neuron -> Neuron -> Neuron
connectToBiasFromNeuron bias from =
    case from of
        Neuron v d c ->
            (Connection 0.0 (descriptor bias))
                :: c
                |> Neuron v d


{-| initial weights from bias neurons ok to be zero
-}
connectToBiasFromLayer : Neuron -> Int -> ANN -> ANN
connectToBiasFromLayer bias layer ann =
    layerNeurons ann layer
        |> List.map (connectToBiasFromNeuron bias)
        |> List.foldl (\n a -> Dict.insert (descriptor n) n a) ann



-- ========== Adding Neurons to ANN ==========
-- 4 for folds, better as changer -> acc ->> acc


addNeuron : Neuron -> ANN -> ANN
addNeuron n ann =
    Dict.insert (descriptor n) n ann


add : Neuron -> ANN -> ANN
add neuron ann =
    case (descriptor neuron) of
        ( 1, _ ) ->
            addNeuron neuron ann

        ( layer, _ ) ->
            let
                newNeuron =
                    connectToLayer ann neuron (layer - 1)
            in
                addNeuron newNeuron ann


addBias : Layer -> ANN -> ANN
addBias layer ann =
    case layer of
        1 ->
            ann

        _ ->
            let
                prevLayer =
                    layer - 1

                nextPosition =
                    1 + (layerSize ann prevLayer)

                desc =
                    ( prevLayer, nextPosition )

                bias =
                    Neuron 1.0 desc []
            in
                bias
                    |> (flip addNeuron) ann
                    --ah - that's how to use flip!
                    |>
                        connectToBiasFromLayer bias layer


addLayer : Layer -> Int -> ANN -> ANN
addLayer layer layerSize ann =
    List.range 1 layerSize
        |> List.map (\position -> ( layer, position ))
        |> List.map (\desc -> Neuron 0.5 desc [])
        |> List.foldl add ann
        |> addBias layer


{-| ========== Building ANN ==========

    By adding to layers in turn

    Weights for any neurons incoming connections have to be random #'s
    from a normal (gaussian) distribution, with mean 0 and standard
    deviation of 1/(sqrt # incoming connections)

    <= BETTER: randomise the weights after building the net, so
               now done in update

-}
build : Layer -> List Int -> ANN -> ANN
build layer sizes ann =
    case sizes of
        [] ->
            ann

        size :: rest ->
            let
                newAnn =
                    addLayer layer size ann
            in
                build (layer + 1) rest newAnn


buildTarget : Layer -> Int -> Target
buildTarget layer size =
    List.range 1 size
        |> List.foldl (\position target -> Dict.insert ( layer, position ) (Neuron 0.5 ( layer, position ) []) target) Dict.empty


boolToValue : Maybe Bool -> Value
boolToValue bool =
    case bool of
        Just boolTo ->
            if boolTo then
                0.999
            else
                0.001

        Nothing ->
            Debug.log "No boolean value supplied to boolToValue" 0.5


{-|
    should be an easy way to addValues to a target?
-}
buildTargetFromList : Layer -> List Bool -> Target
buildTargetFromList layer values =
    let
        vals =
            Array.fromList values
    in
        List.range 1 (List.length values)
            |> List.foldr (\position target -> Dict.insert ( layer, position ) (Neuron (boolToValue (Array.get (position - 1) vals)) ( layer, position ) []) target) Dict.empty


setInputFromList : List Bool -> ANN -> ANN
setInputFromList ins ann =
    let
        vals =
            Array.fromList ins
    in
        List.range 1 (List.length ins)
            |> List.foldr (\position annUpdated -> Dict.insert ( 1, position ) (Neuron (boolToValue (Array.get (position - 1) vals)) ( 1, position ) []) annUpdated) ann



--========== Random Weights - added after build ===========


setConnectionWeight : Connection -> Weight -> Connection
setConnectionWeight connection newWeight =
    let
        _ =
            Debug.log ("weight " ++ (toString newWeight)) (fromDescriptor connection)
    in
        Connection newWeight (fromDescriptor connection)


swapTuple : ( a, b ) -> ( b, a )
swapTuple ( a, b ) =
    ( b, a )


generateRandomWeights : Seed -> Int -> ( List Float, Seed )
generateRandomWeights seed fanIn =
    let
        stdDev =
            1 / (sqrt (toFloat fanIn))

        gen =
            Random.Float.normal 0.0 stdDev
    in
        step (Random.list fanIn gen) seed


setNeuronConnectionWeights : Neuron -> List Weight -> Neuron
setNeuronConnectionWeights neuron weights =
    mapConnections (\connections -> List.map2 setConnectionWeight connections weights) neuron


mapConnections : (Connections -> Connections) -> Neuron -> Neuron
mapConnections fn neuron =
    case neuron of
        Neuron v d connections ->
            Neuron v d (fn connections)


mapWithSeed : (( Seed, Neuron ) -> ( Seed, Neuron )) -> Seed -> List Neuron -> ( Seed, List Neuron )
mapWithSeed fn seed neurons =
    List.foldl (\neuron ( newSeed, neurons ) -> fn ( newSeed, neuron ) |> Tuple.mapSecond (\n -> n :: neurons))
        ( seed, [] )
        neurons


randomiseNeuronConnectionWeights : ( Seed, Neuron ) -> ( Seed, Neuron )
randomiseNeuronConnectionWeights ( seed, neuron ) =
    case neuron of
        Neuron v d connections ->
            List.length connections
                |> generateRandomWeights seed
                |> Tuple.mapFirst (setNeuronConnectionWeights neuron)
                |> swapTuple



-- 2 remove seed using Tuple.second, so foldl just the neurons from neuronAndSeedListMaker
-- 4 flip params to addNeuron, folds work better with fn : changeable -> accumulator -> accumulator


addToAnn : ( Seed, List Neuron ) -> ( Seed, ANN )
addToAnn pair =
    case pair of
        ( seed, neurons ) ->
            ( seed
            , List.foldl addNeuron Dict.empty neurons
            )


randomiseWeights : Seed -> ANN -> ( Seed, ANN )
randomiseWeights seed ann =
    ann
        |> Dict.values
        |> mapWithSeed randomiseNeuronConnectionWeights seed
        |> addToAnn
