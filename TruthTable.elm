module TruthTable exposing (..)

import Ann exposing (..)
import Types exposing (..)
import Model exposing (..)
import Html exposing (..)


truth : Model -> Html Msg
truth model =
    table model.ann


table : ANN -> Html Msg
table ann =
    Html.table [] (tableHead :: (tableRows ann))


tableHead : Html Msg
tableHead =
    Html.thead []
        [ text "Results for ????" ]


tableRows : ANN -> List (Html Msg)
tableRows ann =
    let
        ins =
            List.map stateCell (inputStates ann)

        outs =
            List.map stateCell (outputStates ann)
    in
        [ Html.tr [] ((text "Lesson") :: ins)
        , Html.tr [] ((text "Responses") :: outs)
        ]


stateText : State -> Html Msg
stateText state =
    case state of
        Truthy ->
            text "1"

        Falsey ->
            text "0"

        Unknown value ->
            text (toString value)


boolText : Bool -> Html Msg
boolText bit =
    case bit of
        True ->
            text "1"

        False ->
            text "0"


stateIsSame : Bool -> State -> Bool
stateIsSame bool state =
    case state of
        Truthy ->
            bool

        Falsey ->
            not bool

        Unknown val ->
            False


stateCell : State -> Html Msg
stateCell state =
    Html.td [] [ stateText state ]


{-| fudging an error 20% works quicker than 10%
-}
neuronState : Neuron -> State
neuronState neuron =
    case neuron of
        Neuron value _ _ ->
            value |> state 20


{-| the last input layer neuron is bias for next layer's neurons
-}
inputStates : ANN -> List State
inputStates ann =
    let
        ns =
            layerNeurons ann 1

        bias =
            List.length ns
    in
        ns
            |> List.take (bias - 1)
            |> List.map neuronState


{-|  no next layer, so no bias in output layer
-}
outputStates : ANN -> List State
outputStates ann =
    outputLayerNeurons ann
        |> List.map neuronState
