module Model exposing (..)

-- import Update exposing (Msg)

import Dict exposing (..)
import Ann exposing (..)
import Types exposing (..)
import Time exposing (..)
import AnimationFrame exposing (..)
import Random exposing (..)


type alias Model =
    { layerSizes : List Int
    , runs : Int
    , learning : Bool
    , supervisedLearning : Bool
    , operation : List Bool -> List Bool
    , seed : Seed
    , ann : ANN
    , target : Target
    }


type Msg
    = Sizes String
    | Toggle Neuron
    | ToggleTarget Neuron
    | FF
    | BP
    | Reset
    | Runs String
    | Cycle Time
    | Learn
    | SupervisedCycle Time
    | SupervisedLearning
    | Operator String


{-|
  model passed so can change what we're subscribing to
  so when not learning, dont animate?
  - especially since subscribing to diffs sets the fans off!



-}
subscriptions : Model -> Sub Msg
subscriptions model =
    []
        |> addIfSet (AnimationFrame.diffs Cycle) model.learning
        |> addIfSet (AnimationFrame.diffs SupervisedCycle) model.supervisedLearning
        |> Sub.batch


addIfSet : Sub Msg -> Bool -> List (Sub Msg) -> List (Sub Msg)
addIfSet sub add subs =
    if add then
        sub :: subs
    else
        subs


{-| default operation ... AND
-}
operation : List Bool -> List Bool
operation ins =
    [ List.foldr (&&) True ins ]


model : Model
model =
    Model [] 0 False False operation (initialSeed 1) Dict.empty Dict.empty


init : ( Model, Cmd Msg )
init =
    ( model, Cmd.none )
