module Visualisation exposing (..)

import Ann exposing (..)
import Types exposing (..)
import Update exposing (..)
import Model exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Dict exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)


--import Color
--import Main exposing (Msg)
-- === Visualisation ===


neuronColour : Neuron -> String
neuronColour neuron =
    case neuron of
        Neuron value _ _ ->
            let
                v =
                    toString (256 * value)

                v1 =
                    toString (value)
            in
                --toString (rgba 255 0 0 v1)
                -- how the fuck does this work?
                "rgba(255, 0, 0, " ++ v1 ++ ")"


connectionColour : Connection -> String
connectionColour connection =
    case connection of
        Connection weight _ ->
            if weight > 0 then
                "#FF0000"
            else
                "#0000FF"


neuronRadius : String
neuronRadius =
    "10"


connectionSvg : Descriptor -> Connection -> Svg msg
connectionSvg to from =
    let
        ( tox, toy ) =
            to

        ( fromx, fromy ) =
            fromDescriptor from

        weight =
            fromWeight from
    in
        line
            [ stroke (connectionColour from)
              --weightColour weight
            , x1 (toString (fromx * 50))
            , y1 (toString (fromy * 50))
            , x2 (toString (tox * 50))
            , y2 (toString (toy * 50))
            ]
            []


neuronSvg : Neuron -> Svg Msg
neuronSvg neuron =
    let
        ( layer, idx ) =
            descriptor neuron
    in
        circle
            [ fill (neuronColour neuron)
            , cx (toString (layer * 50))
            , cy (toString (idx * 50))
            , r neuronRadius
            , Svg.Events.onClick (Toggle neuron)
            ]
            []


{-|
Ok - so I am being lazy! Only thing defferent is onClick Msg
-}
targetNeuronSvg : Neuron -> Svg Msg
targetNeuronSvg neuron =
    let
        ( layer, idx ) =
            descriptor neuron
    in
        circle
            [ fill (neuronColour neuron)
            , cx (toString (layer * 50))
            , cy (toString (idx * 50))
            , r neuronRadius
            , Svg.Events.onClick (ToggleTarget neuron)
            ]
            []


connectionsSvg : Neuron -> List (Svg Msg)
connectionsSvg to =
    connections to
        |> List.map (connectionSvg (descriptor to))


canvas : String -> List (Html.Attribute Msg)
canvas dimensions =
    [ version "1.1", x "0", y "0", viewBox dimensions ]


modelSvg : String -> Model -> Html Msg
modelSvg dimensions model =
    let
        olayer =
            outputLayer model.ann

        netNeurons =
            Dict.values model.ann

        targetNeurons =
            Dict.values model.target
                |> List.map (moveToLayer (olayer + 1))
    in
        svg (canvas dimensions)
            (annSvg netNeurons
                ++ targetSvg targetNeurons
            )


moveToLayer : Layer -> Neuron -> Neuron
moveToLayer layer neuron =
    case neuron of
        Neuron value ( _, position ) connections ->
            Neuron value ( layer, position ) connections


annSvg : List Neuron -> List (Svg Msg)
annSvg netNeurons =
    (netNeurons
        |> List.map connectionsSvg
        |> List.concat
    )
        ++ (netNeurons
                |> List.map neuronSvg
           )


targetSvg : List Neuron -> List (Svg Msg)
targetSvg targetNeurons =
    targetNeurons
        |> List.map targetNeuronSvg
