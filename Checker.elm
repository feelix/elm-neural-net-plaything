module Checker exposing (..)

import Model exposing (..)
import Lesson exposing (..)
import TruthTable exposing (..)
import Ann exposing (..)
import Network exposing (..)
import Types exposing (..)
import Boolean exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)


{-|
So: a net is learning a particular operator...
generate sets of tests covering all input  permutations
get the result given by a net for each input
and the result from the operator being learnt
and display these results as a truth table in html
-}
check : Model -> Html Msg
check model =
    Html.div []
        [ Html.caption [] [ text "Caption" ]
        , Html.table [ style [ ( "border", "1px solid black" ) ] ]
            (List.concat
                [ [ checkHeadings model.ann ]
                , checkRows model.operation model.ann
                , [ checkFooter model.ann ]
                ]
            )
        ]


checkHeadings : ANN -> Html Msg
checkHeadings ann =
    let
        inCols =
            inputLayerSize ann

        outCols =
            outputLayerSize ann

        expectedCols =
            outCols

        totalCols =
            inCols + outCols + expectedCols
    in
        Html.thead []
            [ tr [] [ th [ colspan totalCols ] [ text "Checker Truth Table" ] ]
            , tr []
                [ th [ colspan inCols ] [ text "Inputs" ]
                , th [ colspan outCols ] [ text "Outputs" ]
                , th [ colspan expectedCols ] [ text "Expected" ]
                ]
            ]


{-|
- generate the tests
- run each through ann
- build table row
-}
checkRows : (List Bool -> List Bool) -> ANN -> List (Html Msg)
checkRows fn ann =
    ann
        |> runTests fn
        |> List.map checkRow


runTests : (List Bool -> List Bool) -> ANN -> List ( List State, List State, List Bool )
runTests fn ann =
    let
        seed =
            allFalse (inputLayerSize ann)
    in
        let
            tests =
                genAll [ seed ]

            _ =
                Debug.log (toString tests)
        in
            tests
                |> List.map (runTest fn ann)


runTest : (List Bool -> List Bool) -> ANN -> List Bool -> ( List State, List State, List Bool )
runTest fn ann ins =
    let
        after =
            ann
                |> setInputFromList ins
                |> annFF
    in
        ( inputStates after
        , outputStates after
        , fn ins
        )


checkRow : ( List State, List State, List Bool ) -> Html Msg
checkRow ( ins, outs, expected ) =
    tr []
        (List.concat
            [ List.map toCell ins
            , List.map toCell outs
            , List.map2 expectedCell expected outs
            ]
        )


checkFooter : ANN -> Html Msg
checkFooter ann =
    text "footer"


expectedCell : Bool -> State -> Html Msg
expectedCell bool state =
    let
        correct =
            stateIsSame bool state
    in
        td
            (if correct then
                [ style [ ( "backgroundColor", "green" ) ] ]
             else
                [ style [ ( "backgroundColor", "red" ) ] ]
            )
            [ boolText bool ]



--   [ boolToString bool]


toCell : State -> Html Msg
toCell state =
    td [ style [ ( "border", "1px solid black" ), ( "width", "50px" ) ] ] [ stateText state ]


boolToString : Bool -> String
boolToString b =
    if b then
        "1"
    else
        "0"


currentFooter : ANN -> Html Msg
currentFooter ann =
    text "current lesson"
