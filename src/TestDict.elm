module TestDict exposing (..)

import Dict exposing (..)


type alias TestDict =
    Dict ( Row, Col ) Int


type alias Row =
    Int


type alias Col =
    Int


buildTuple : Row -> Col -> ( Row, Col )
buildTuple row col =
    ( row, col )


buildRowColTuples : Row -> List ( Row, Col )
buildRowColTuples row =
    List.range 1 10
        |> List.map (\n -> 11 - n)
        |> List.map (buildTuple row)


build : TestDict
build =
    List.range 1 10
        |> List.map (\n -> 11 - n)
        |> List.map buildRowColTuples
        |> List.concat
        |> List.foldr (\( r, c ) d -> Dict.insert ( r, c ) (100 - (((r - 1) * 10) + c)) d) Dict.empty
