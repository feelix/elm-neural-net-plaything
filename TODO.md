##just done...
- allow selection of the operator function to learn
- truth table showing last result

##Soon Come...(in no particular order)##
- Use Blue/Red with alpha for activations - so can see if more 1-ish or 0-ish
- Use alpha for the weights, so can see relative strength
- show % accuracy over all runs so far

##Did Come... (but was abandoned, only experimental)##
###Super-fast running of the learning cycle###
As a flat-out speed test I hacked the update loop (by using Task.perform in a bad way) to generate a Cycle Cmd Msg after each learn and feed it back straight into update. It ran very fast of course but was glitchy and hardly ever actually got re-rendered.

###Where are we and where are we going###
- && || NAND all learned by ANN in <1000 lessons, learning rate of 1
- XOR in <2000 with visually 'good' looking intial weights
- Inverse, takes a lot more lessons for 4 -> 4, but works.
