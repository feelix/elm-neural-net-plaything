module Lesson exposing (..)

import Types exposing (..)
import Ann exposing (..)
import Model exposing (..)
import Network exposing (..)
import Boolean exposing (..)
import Random exposing (..)
import List.Extra exposing (..)


learn : Model -> Model
learn model =
    let
        outLayer =
            outputLayer model.ann

        inSize =
            (layerSize model.ann 1) - 1

        ( ins, newSeed ) =
            generateLesson model.seed inSize

        outs =
            model.operation ins

        expected =
            buildTargetFromList (outLayer + 1) outs
    in
        { model
            | seed = newSeed
            , ann = learnCycle model.ann ins expected
            , target = expected
            , runs = model.runs - 1
            , supervisedLearning = (model.runs > 0) && model.supervisedLearning
        }


{-|
    just using AND for testing
-}
opName : Int -> String
opName number =
    case number of
        1 ->
            "AND"

        2 ->
            "OR"

        3 ->
            "NAND"

        4 ->
            "XOR"

        5 ->
            "OLD AND (parity?)"

        6 ->
            "halfadd"

        7 ->
            "INVERT"

        _ ->
            "AND"


makeOperation : (Bool -> Bool -> Bool) -> List Bool -> List Bool
makeOperation op bits =
    let
        result =
            List.Extra.foldl1 op bits
                |> Maybe.withDefault True
    in
        [ result ]


{-| && for now
-}
parity : List Bool -> List Bool
parity ins =
    operation ins


halfAdd : List Bool -> List Bool
halfAdd ins =
    let
        ones =
            List.filter identity ins
                |> List.length
    in
        case ones of
            3 ->
                [ True, True ]

            2 ->
                [ False, True ]

            1 ->
                [ True, False ]

            0 ->
                [ False, False ]

            _ ->
                Debug.crash "Too many inputs to half adder"


invert : List Bool -> List Bool
invert values =
    List.map not values


getOperation : String -> (List Bool -> List Bool)
getOperation count =
    let
        number =
            Result.withDefault 1 (String.toInt count)
    in
        case number of
            1 ->
                makeOperation (&&)

            2 ->
                makeOperation (||)

            3 ->
                makeOperation (&&) >> invert

            4 ->
                makeOperation xor

            5 ->
                parity

            6 ->
                halfAdd

            7 ->
                invert

            _ ->
                makeOperation (&&)


generateLesson : Seed -> Int -> ( List Bool, Seed )
generateLesson seed ins =
    step (boolListGenerator ins) seed


boolListGenerator : Int -> Generator (List Bool)
boolListGenerator ins =
    list ins bool


learnCycle : ANN -> List Bool -> Target -> ANN
learnCycle ann ins target =
    ann
        |> setInputFromList ins
        |> annLearn target
