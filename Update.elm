module Update exposing (..)

import Lesson exposing (..)
import Types exposing (..)
import Ann exposing (..)
import Network exposing (annFF, annBP, annLearn)
import Dict exposing (..)
import Model exposing (Model, Msg)
import Platform.Cmd exposing (..)
import Time exposing (..)


toList : String -> List Int
toList csv =
    String.split "," csv
        |> List.map (\num -> Result.withDefault 0 (String.toInt num))


toggle : Value -> Value
toggle value =
    if value > 0.5 then
        0.001
    else
        0.999


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Model.FF ->
            ( { model | ann = annFF model.ann }, Cmd.none )

        Model.BP ->
            ( { model | ann = annBP model.ann model.target }, Cmd.none )

        Model.Reset ->
            let
                ( newSeed, newAnn ) =
                    randomiseWeights model.seed model.ann
            in
                ( { model
                    | ann = newAnn
                    , seed = newSeed
                  }
                , Cmd.none
                )

        Model.Operator name ->
            ( { model | operation = getOperation name }, Cmd.none )

        Model.Learn ->
            ( { model | learning = not model.learning }, Cmd.none )

        Model.Cycle diff ->
            if (model.learning && (model.runs > 1)) then
                ( { model
                    | ann = annLearn model.target model.ann
                    , runs = model.runs - 1
                    , learning = (model.runs > 0) && model.learning
                  }
                , Cmd.none
                )
            else
                ( model, Cmd.none )

        Model.SupervisedLearning ->
            ( { model | supervisedLearning = not model.supervisedLearning }, Cmd.none )

        Model.SupervisedCycle diff ->
            if (model.supervisedLearning && (model.runs > 1)) then
                ( Lesson.learn model, Cmd.none )
            else
                ( { model | supervisedLearning = not model.supervisedLearning }, Cmd.none )

        Model.Runs lessons ->
            ( { model | runs = Result.withDefault 0 (String.toInt lessons) }, Cmd.none )

        Model.Toggle neuron ->
            case neuron of
                Neuron value descriptor connections ->
                    ( { model | ann = Dict.insert descriptor (Neuron (toggle value) descriptor connections) model.ann }, Cmd.none )

        Model.ToggleTarget neuron ->
            case neuron of
                Neuron value descriptor connections ->
                    ( { model | target = Dict.insert descriptor (Neuron (toggle value) descriptor connections) model.target }, Cmd.none )

        --setNeuronValue model.ann neuron (toggle (ANN2.value neuron)) }
        Model.Sizes newLayerSizes ->
            let
                s =
                    toList newLayerSizes

                ( newSeed, newAnn ) =
                    build 1 s Dict.empty
                        |> randomiseWeights model.seed

                t =
                    buildTarget (List.length s + 1) (outputLayerSize newAnn)
            in
                ( { model
                    | layerSizes = s
                    , seed = newSeed
                    , ann = newAnn
                    , target = t
                  }
                , Cmd.none
                )
