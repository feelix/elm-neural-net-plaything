module Tests exposing (..)

import Test exposing (..)
import Expect
import Fuzz exposing (list, int, tuple, string)
import String
import Dict exposing (..)
import TestDict exposing (..)


all : Test
all =
    describe "Test of Dict ordering, specifically toList where key is tuple (Int, Int)"
        [ describe "Check basics"
            [ test "build size should be 100 cells" <|
                \() ->
                    Expect.equal (Dict.size build) 100
            , test "values should be in ascending order" <|
                \() ->
                    Expect.equal (Dict.values build) (List.range 1 100)
            ]
        , describe "Fuzz test examples, using randomly generated input"
            [ fuzz (list int) "Lists always have positive length" <|
                \aList ->
                    List.length aList |> Expect.atLeast 0
            ]
        ]
